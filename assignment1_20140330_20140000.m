% 10 runs
sum = 0;
for run = 1:10
    [x, c1, c2, profit] = simulate();
    sum = sum + profit;
end

disp(sum/10);

sum = 0;
for run = 1:1000000
    [x, c1, c2, profit] = simulate();
    sum = sum + profit;
end

disp(sum/1000000);




% logic function
function [x, c1, c2, profit] = simulate()
    % constants
    sellingPrice = 249;
    adminCost = 400000;
    adCost = 600000;

    % constants for x and c1
    a = 80;
    b = 100;
    mu = 15000;
    sigma = 4500;
    
    % c2 distribution
    keys = {0.1, 0.3, 0.7, 0.9, 1};
    values = {43, 44, 45, 46, 47};
    c1Map = containers.Map(keys, values);
    
    % variables
    x = normrnd(mu, sigma); % demanded printers
    temp = rand;
    c1RandValue = 0;

    for i = 1:numel(keys)
        if temp < keys{i}
            c1RandValue = c1Map(keys{i});
            break;
        end
    end

    c1 = c1RandValue; % labor cost
    c2 = (rand * (b - a)) + a; % parts cost

    % equation
    profit = (x * (sellingPrice - c1 - c2)) - adminCost - adCost;
   
end